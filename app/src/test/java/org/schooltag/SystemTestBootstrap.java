package org.schooltag;

import com.google.firebase.database.DatabaseReference;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.tz.UTCProvider;
import org.schooltag.entity.Audit;

import static org.mockito.Mockito.mock;

/**
 * Use this in unit tests to tie all the services together with default values.
 *
 * Created by aaronroller on 7/22/17.
 */

public class SystemTestBootstrap extends SystemBootstrap {

    static {
        //avoids outputting error
        //https://github.com/dlew/joda-time-android/issues/148
        DateTimeZone.setProvider(new UTCProvider());
        final DateTimeZone zone = DateTimeZone.forOffsetHours(-2);
        DateTimeZone.setDefault(zone);
    }

    public SystemTestBootstrap() {
        super(mock(DatabaseReference.class), DateTimeZone.getDefault(),
                Audit.builder().who("test").when(DateTime.now().toString()).build(),/*firebase auth*/null);
    }


}
