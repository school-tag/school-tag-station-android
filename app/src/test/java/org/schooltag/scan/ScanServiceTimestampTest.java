package org.schooltag.scan;

import com.google.common.base.Optional;
import com.google.common.util.concurrent.FutureCallback;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.schooltag.SystemBootstrap;
import org.schooltag.SystemTestBootstrap;
import org.schooltag.entity.RepositoryUtils;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Matchers.notNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * The Scan service deals with the default timestamps for a scan.
 * Ensure the defaults are being applied and the time zones are correct.
 * Created by aaronroller on 7/22/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ScanServiceTimestampTest {

    private DateTimeZone defaultTimeZone;

    private ScanService scanService;

    @Mock
    private FutureCallback<ScanId> scanCallback;

    @Mock
    private ScanRepository scanRepository;
    private RepositoryUtils utils;


    @Before
    public void setUp() {
        SystemBootstrap bootstrap = new SystemTestBootstrap();
        defaultTimeZone = bootstrap.timeZone;
        this.utils = bootstrap.repositoryUtils;
        when(scanRepository.getUtils()).thenReturn(utils);
        scanService = bootstrap.scanService;
    }

    @Test
    public void scanWithoutTimestampGetsNow() {
        final Scan scanIn = Scan.builder().tagId("foo").build();
        assertThat(scanIn.timestamp, equalTo(null));
        assertScanGetsNow(scanIn);
    }

    private void assertScanGetsNow(final Scan scanIn) {
        Scan scanWithTimestamp = scanService.scanWithTimestamp(scanIn);
        assertThat(scanWithTimestamp.timestamp, notNullValue());
        final Optional<DateTime> dateTimeOptional = scanService.dateTime(scanWithTimestamp);
        assertThat(dateTimeOptional.isPresent(), equalTo(true));
        final DateTime dateTime = dateTimeOptional.get();
        assertThat(dateTime.getZone(), equalTo(defaultTimeZone));
        assertThat(dateTime.toString() + " <= now ", dateTime.isBeforeNow() || dateTime.isEqualNow(), equalTo(true));
    }

    @Test
    public void scanWithTimestampProvidedNow() {
        assertScanGetsNow(Scan.builder().tagId("foo").timestamp("this will get clobbered").build());
    }

    @Test
    public void scanReceivedSetsTimestamp() {
        final ScanService scanService = ScanService.builder().repository(scanRepository).build();
        ScanService spy = spy(scanService);
        final Scan scan = Scan.builder().build();
        spy.scanReceived(scan, scanCallback);
        verify(spy, times(1)).scanWithTimestamp(scan);

    }

}
