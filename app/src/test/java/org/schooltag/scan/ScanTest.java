package org.schooltag.scan;

import org.joda.time.DateTimeZone;
import org.junit.Before;
import org.junit.Test;

import org.schooltag.entity.EntityUtils;
import org.schooltag.tag.TagId;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by aaronroller on 7/18/17.
 */

public class ScanTest {






    @Test
    public void toBuilderKeepsValues() {
        Scan scan = Scan.builder().tagId("tag-optionalId").id("scan-optionalId").build();
        final Scan scan1 = scan.toBuilder().build();
        assertThat(scan.tagId(), equalTo(scan1.tagId()));
        assertThat(scan.id(), equalTo(scan1.id()));
    }

    @Test
    public void nullIdIsOk() {
        Scan scan = Scan.builder().tagId("foo").build();
        assertThat(scan.id().isPresent(), is(false));
        assertThat(EntityUtils.optionalToString(scan.id()), equalTo(null));
    }
}
