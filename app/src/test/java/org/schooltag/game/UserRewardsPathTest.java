package org.schooltag.game;

import android.support.annotation.NonNull;

import org.hamcrest.MatcherAssert;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.schooltag.SystemTestBootstrap;
import org.schooltag.entity.Path;
import org.schooltag.user.UserId;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by aaronroller on 7/22/17.
 */

public class UserRewardsPathTest {


    UserId user = new UserId("/some/random/root/users/foo");
    private SystemTestBootstrap bootstrap;

    @Before
    public void setup() {
        this.bootstrap = new SystemTestBootstrap();
    }

    @Test
    public void rewards() {
        MatcherAssert.assertThat(rewarded().toString(), is(equalTo("/rules")));
    }


    @Test
    public void userRewards() {

        MatcherAssert.assertThat(rewardedToUser().toString(), is(equalTo("/rules/users/foo")));
    }

    private UserRewardsPath.TotalsPath rewardedToUser() {
        return rewarded().to(user);
    }

    @Test
    public void dailyUserRewards() {

        final int year = 2017;
        final int week = 23;
        final int day = 3;
        DateTime dateTime = bootstrap.repositoryUtils.now().withYear(year).withWeekOfWeekyear(week).withDayOfWeek(day);
        final Path rewardsForDay = rewardedToUser().onDay(dateTime);
        assertThat(rewardsForDay.toString(), equalTo("/rules/users/foo/totals/dailies/years/2017/weeks/23/days/3"));
    }

    @NonNull
    private UserRewardsPath rewarded() {
        return UserRewardsPath.rewarded();
    }

}
