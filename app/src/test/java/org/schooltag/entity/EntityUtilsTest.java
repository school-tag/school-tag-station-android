package org.schooltag.entity;

import com.google.common.base.Optional;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Created by aaronroller on 7/20/17.
 */

public class EntityUtilsTest {

    @Test
    public void optionalAbsentToStringIsNull() {
        assertThat(EntityUtils.optionalToString(Optional.fromNullable(null)), equalTo(null));
    }

    @Test
    public void optionalPresentToStringIsValue() {
        assertThat(EntityUtils.optionalToString(Optional.of(23)), equalTo("23"));
    }
}
