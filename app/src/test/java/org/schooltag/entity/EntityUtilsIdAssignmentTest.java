package org.schooltag.entity;


import com.google.common.base.Optional;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.schooltag.entity.EntityTestUtils.*;

import org.junit.Test;
import org.junit.runners.model.TestClass;

/**
 * Tests the
 * Created by aaronroller on 8/20/17.
 */

public class EntityUtilsIdAssignmentTest {

    @Test(expected = NoSuchMethodException.class)
    public void missingToBuilderShouldThrowException() throws NoSuchMethodException {
        EntityUtils.idAssignmentFunction(TestEntity2.class);
    }

    @Test
    public void idAssignedSuccessfully() throws NoSuchMethodException {
        final TestEntity testEntity = TestEntity.builder().build();

        EntityUtils.EntityIdPair<TestEntity, TestId> pair = new EntityUtils.EntityIdPair<>(testEntity, id);
        final TestEntity withId = EntityUtils.idAssignmentFunction(TestEntity.class).apply(pair);
        assertNotNull(withId);
        final Optional<TestId> idOptional = withId.id();
        assertThat(idOptional.isPresent(), equalTo(true));
        assertThat(idOptional.get(), equalTo(EntityTestUtils.id));
    }

}
