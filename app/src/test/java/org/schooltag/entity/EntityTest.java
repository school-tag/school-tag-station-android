package org.schooltag.entity;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.schooltag.entity.EntityTestUtils.TestEntity;
import static org.schooltag.entity.EntityTestUtils.TestEntity2;
import static org.schooltag.entity.EntityTestUtils.TestId;
import static org.schooltag.entity.EntityTestUtils.TestId2;
import static org.schooltag.entity.EntityTestUtils.id;
import static org.schooltag.entity.EntityTestUtils.idValue;
import static org.schooltag.entity.EntityTestUtils.idValue2;


/**
 * Created by aaronroller on 7/15/17.
 */

public class EntityTest {


    @Test
    public void entityIdShouldBeAccessible() {

        final TestEntity entity = new TestEntity(idValue);
        assertThat(entity.id().get(), equalTo(id));
        assertThat(entity.id().get().toString(), equalTo(id.toString()));
    }

    @Test
    public void idIsOptonal() {
        assertThat(new TestEntity(null).id().isPresent(), equalTo(false));
        assertThat(new TestEntity(idValue).id().isPresent(), equalTo(true));
    }

    @Test
    public void idsMustMeetEqualityCriteria() {

        final TestId id1 = new TestId(idValue);
        final TestId id2 = new TestId(idValue);
        final TestId2 id3 = new TestId2(idValue);
        assertThat(id1, not(sameInstance(id2)));
        assertThat(id1, equalTo(id2));
        assertThat(id1.toString(), equalTo(idValue));
        assertThat(new TestEntity(id1.toString()), equalTo(new TestEntity(id2.toString())));
        assertThat(id1.equals(id3), equalTo(false));
        assertThat(id1, not(equalTo(new TestId(idValue2))));
    }

    @Test
    public void entitiesWithSameIdsShouldBeEqual() {
        Entity entity1 = new TestEntity(idValue);
        Entity entity2 = new TestEntity(idValue);
        assertThat(entity1.equals(entity2), equalTo(true));
    }

    @Test
    public void entitiesWithDifferentIdsShouldNotBeEqual() {
        Entity entity1 = new TestEntity(idValue);
        Entity entity2 = new TestEntity(idValue2);
        assertThat(entity1.equals(entity2), equalTo(false));
    }

    @Test
    public void differententitiesWithSameIdsShouldNotBeEqual() {
        Entity entity1 = new TestEntity(idValue);
        Entity entity2 = new TestEntity2(new TestId2(idValue));
        assertThat(entity1.equals(entity2), equalTo(false));
    }

    @Test
    public void idKeyIsTheLastElement(){
        TestId id = new TestId("first/second/third");
        assertThat(id.key(),equalTo("third"));
    }

}
