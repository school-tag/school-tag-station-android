package org.schooltag.entity;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by aaronroller on 7/21/17.
 */

public class PathTest {
    //small l looks similar to a slash
    public static final String l = Path.DELIMITER;
    public static final String ONE_PATH = "/one";
    final String one = "one";
    final String two = "two";

    @Test
    public void emptyPath() {
        assertThat(Path.from().toString(), equalTo(l));
    }

    @Test
    public void providedSlashShouldBeAlone() {
        assertThat(Path.from(l).toString(), equalTo(l));
    }

    @Test
    public void givenThreeElementPathUnchaned() {
        final String expected = "/one/two/three";
        assertThat(Path.from(expected).toString(), equalTo(expected));
    }

    @Test
    public void trailingSlashRemoved() {
        assertThat(Path.from("/one/").toString(), equalTo(ONE_PATH));
    }

    @Test
    public void oneElementPath() {
        assertThat(Path.from(one).toString(), equalTo(ONE_PATH));
    }

    @Test
    public void twoElementPath() {
        assertThat(Path.from(one).and(two).toString(), equalTo("/one/two"));
    }

    @Test
    public void pathRootAndOne() {
        assertThat(Path.from().and(one).toString(), equalTo(ONE_PATH));
    }
}
