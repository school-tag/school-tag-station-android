package org.schooltag.entity;

import com.google.firebase.database.DatabaseReference;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.schooltag.SystemTestBootstrap;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @see RepositoryUtils#path(DatabaseReference)
 * <p>
 * Created by aaronroller on 7/22/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class RepositoryUtilsPathFromReferenceTest {

    private RepositoryUtils utils;
    @org.mockito.Mock
    DatabaseReference reference;

    @Before
    public void setUp() {
        SystemTestBootstrap bootstrap = new SystemTestBootstrap();
        this.utils = RepositoryUtils.builder().timeZone(bootstrap.timeZone).build();
    }


    @Test
    public void rootReferenceRootPath() {
        mockRoot(this.reference);
        final String expected = "/";
        assertPath(expected);
    }

    @Test
    public void topLevelPath() {
        final DatabaseReference reference = this.reference;
        a(reference);
        assertPath("/a");
    }


    @Test
    public void twoLevelPath() {
        when(reference.getKey()).thenReturn("b");
        DatabaseReference a = mockRef();
        a(a);
        when(reference.getParent()).thenReturn(a);
        assertPath("/a/b");

    }

    private void assertPath(final String expected) {
        assertThat(utils.path(reference).toString(), equalTo(expected));
    }

    private static void mockRoot(final DatabaseReference reference) {
        when(reference.getKey()).thenReturn(null);
    }

    private void a(final DatabaseReference reference) {
        when(reference.getKey()).thenReturn("a");
        final DatabaseReference parent = mockRef();
        mockRoot(parent);
        when(reference.getParent()).thenReturn(parent);
    }

    private DatabaseReference mockRef() {
        return mock(DatabaseReference.class);
    }

}
