package org.schooltag.entity;

import android.support.annotation.Nullable;

import com.google.common.base.Function;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.core.IsSame.sameInstance;
import static org.junit.Assert.assertThat;

/**
 * Created by aaronroller on 8/20/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class RepositoryTest {

    @Mock
    RepositoryUtils utils;
    @Mock
    Audit auditTemplate;

    @Test
    public void idAssignmentAssignedIfNotProvided() {
        final Repository repository = builder()
                .entityType(EntityTestUtils.TestEntity.class)//allows default to be provided
                .build();
        assertThat(repository.idAssignmentFunction, notNullValue());

    }

    private Repository.RepositoryBuilder builder() {
        return Repository.builder()
                .utils(utils)
                .auditTemplate(auditTemplate);
    }

    @Test
    public void idAssignmentNullIfEntityTypeNotProvided() {
        final Repository repository = builder().build();
        assertThat(repository.idAssignmentFunction, nullValue());
    }

    @Test
    public void idAssigmentIfGivenIsUsed() {
        Function given = new Function() {
            @Nullable
            @Override
            public Object apply(final Object input) {
                return null;
            }
        };
        final Repository repository = builder().idAssignmentFunction(given).build();
        assertThat(repository.idAssignmentFunction, sameInstance(given));
    }
}
