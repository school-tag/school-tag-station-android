package org.schooltag.entity;

import lombok.Builder;

/**
 * Created by aaronroller on 8/20/17.
 */

public class EntityTestUtils {

    static public final String idValue = "foo";
    static public final String idValue2 = "bar";
    static public TestId id = new TestId(idValue);


    static class TestEntity extends Entity<TestEntity, TestId> {
        @Builder(toBuilder = true)
        TestEntity(String id) {
            super(id);
        }
    }

    static class TestEntity2 extends Entity<TestEntity2, TestId2> {
        //BUilder not supported
        TestEntity2(TestId2 id) {
            super(id.toString());
        }
    }



    static class TestId extends Id<TestEntity> {
        public TestId(String value) {
            super(value);
        }
    }

    static class TestId2 extends Id<TestEntity2> {
        public TestId2(String value) {
            super(value);
        }
    }


}
