package org.schooltag.tag;

import android.net.Uri;

import org.junit.Test;
import org.schooltag.scan.Scan;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

/**
 * Created by aaronroller on 1/3/18.
 */

public class TagUriTest {

    final Uri validUri = Uri.parse("https://sandbox.schooltag.org/scan/backpack/tag?m=tag-uri-test");
    @Test
    public void backbackTagUriProducesTypeAndCategory(){
        final TagUri tagUri = TagUri.valueOf(validUri);
        assertThat(tagUri,not(equalTo(null)));
        assertThat(tagUri.category, equalTo("backpack"));
        assertThat(tagUri.type, equalTo("tag"));
    }
}
