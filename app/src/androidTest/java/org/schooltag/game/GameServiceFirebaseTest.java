package org.schooltag.game;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.common.collect.Iterables;
import com.google.common.util.concurrent.FutureCallback;

import org.junit.Before;
import org.junit.Test;
import org.schooltag.FirebaseDatabaseTestBase;
import org.schooltag.scan.Scan;
import org.schooltag.user.User;
import org.schooltag.user.UserId;

import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.annotation.Nullable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Integration test using the firebase functions to calculate the totals using firebase
 * to notify of the data change.
 *
 * More granular integration tests are not possible since the android client doesn't have the need
 * to create user rules and other entities from the client so it relies on the server.
 *
 * Created by aaronroller on 7/22/17.
 */

public class GameServiceFirebaseTest extends FirebaseDatabaseTestBase {

    private static final String TAG = GameServiceFirebaseTest.class.getSimpleName();

    public GameServiceFirebaseTest() {
        super(TestType.INTEGRATION);
    }

    Scan scan;

    @Before
    public void setUp() {
        //to prove everything is being created correctly,
        // we must use a new scan id each time to get a new user
        scan = Scan.builder().tagId(String.valueOf(new Random().nextLong())).build();
    }

    @Test
    public void scanReceivedProvidesUserId() {


        bootstrap.gameService.userIdForScanReceived(scan, new FutureCallback<UserScan>() {
            @Override
            public void onSuccess(@Nullable final UserScan userScan) {
                assertThat(userScan.userId, not(equalTo(null)));
                assertThat(userScan.scanId, not(equalTo(null)));
                testCompletedSuccessfully();
            }

            @Override
            public void onFailure(final Throwable t) {
                //this happens sometimes because a user is just getting created.
                //let the timeout fail the test, not receiving a not found event.
                warn(t);
            }
        });
    }

    private void warn(final Throwable t) {
        Log.w(TAG, t.getMessage());
    }


    @Test
    public void scanReceivedProvidesDailyRewards() {


        final FutureCallback<UserRewards> callback = userRewardsCallback();

        bootstrap.gameService.userIdForScanReceived(scan, new FutureCallback<UserScan>() {
            @Override
            public void onSuccess(@Nullable final UserScan userScan) {
                bootstrap.gameService.userRewardsForToday(userScan.userId, callback);
            }

            @Override
            public void onFailure(final Throwable t) {
                warn(t);
            }
        });
    }

    @NonNull
    private FutureCallback<UserRewards> userRewardsCallback() {
        return new FutureCallback<UserRewards>() {

            @Override
            public void onSuccess(@Nullable final UserRewards result) {
                assertThat(result.points, is(greaterThan(0)));
                testCompletedSuccessfully();
            }

            @Override
            public void onFailure(final Throwable t) {
                warn(t);
            }
        };
    }

    @Test
    public void scanReceivedProvidesWeeklyRewards() {

        final FutureCallback<UserRewards> callback = userRewardsCallback();

        bootstrap.gameService.userIdForScanReceived(scan, new FutureCallback<UserScan>() {
            @Override
            public void onSuccess(@Nullable final UserScan userScan) {
                bootstrap.gameService.userRewardsForWeek(userScan.userId, callback);
            }

            @Override
            public void onFailure(final Throwable t) {
                warn(t);
            }
        });
    }


    @Test
    public void rewardsItemizedForScanReceived() {
        bootstrap.gameService.userIdForScanReceived(scan, new FutureCallback<UserScan>() {
            @Override
            public void onSuccess(@Nullable final UserScan userScan) {
                bootstrap.gameService.scanRewardsForToday(userScan.userId, itemizedScanCallback(userScan));
            }

            @Override
            public void onFailure(final Throwable t) {
                warn(t);
            }
        });
    }

    @NonNull
    private FutureCallback<List<ScanReward>> itemizedScanCallback(final @Nullable UserScan userScan) {
        return new FutureCallback<List<ScanReward>>() {
            @Override
            public void onSuccess(@Nullable final List<ScanReward> result) {
                assertThat(result, notNullValue());
                if (!result.isEmpty()) {
                    assertThat(result, not(empty()));

                    for (ScanReward scanReward :
                            result) {
                        assertThat(scanReward.id().isPresent(), equalTo(true));
                        assertThat(scanReward.id().get().scanKey(), equalTo(userScan.scanId.scanKey()));
                        assertThat(scanReward.rules, notNullValue());
                        assertThat(scanReward.rules.entrySet(), hasSize(1));
                        final Map.Entry<String, Reward> first = Iterables.getFirst(scanReward.rules.entrySet(), null);
                        assertThat(first, notNullValue());
                        Reward reward = first.getValue();
                        assertThat(reward.points, notNullValue());
                    }

                    testCompletedSuccessfully();
                } else {
                    Log.d(TAG, "waiting for scan rewards to be updated");
                }
            }

            @Override
            public void onFailure(final Throwable t) {
                testCompleted(t);
            }
        };
    }

    @Test
    public void scanProvidesManyCallbacks() {


        final FutureCallback callbackReceivedCallback = new FutureCallback() {
            boolean dailyReceived = false;
            boolean weeklyReceived = false;
            boolean scanItemsReceived = false;
            boolean userReceived = false;

            @Override
            public void onSuccess(@Nullable final Object result) {

                if (result instanceof UserRewards) {
                    UserRewards.IntervalType intervalType = ((UserRewards) result).intervalType;
                    switch (intervalType) {
                        case DAILY:
                            dailyReceived = true;
                            break;
                        case WEEKLY:
                            weeklyReceived = true;
                            break;
                        default:
                            testCompleted("received an internval type not handled " + intervalType);
                    }
                } else if (result instanceof List) {
                    scanItemsReceived = true;
                } else if (result instanceof User) {
                    User user = (User) result;
                    assertThat(user.id().isPresent(), equalTo(true));
                    userReceived = true;
                }else{
                    testCompleted(result + " received, but not handled");
                }

                if(dailyReceived && weeklyReceived && scanItemsReceived && userReceived){
                    testCompletedSuccessfully();
                }else{
                    Log.d(TAG,"daily: " + dailyReceived +
                            "weekly: " + weeklyReceived +
                            "items: " + scanItemsReceived +
                            "user: " + userReceived
                    );
                }
            }

            @Override
            public void onFailure(final Throwable t) {
                warn(t);
            }
        };


        bootstrap.gameService.userIdForScanReceived(scan, new FutureCallback<UserScan>() {
            @Override
            public void onSuccess(@Nullable final UserScan userScan) {
                bootstrap.gameService.rewardsForScanReceived(
                        scan,
                        callbackReceivedCallback,
                        callbackReceivedCallback,
                        callbackReceivedCallback,
                        callbackReceivedCallback,
                        callbackReceivedCallback);
            }

            @Override
            public void onFailure(final Throwable t) {
                warn(t);
            }
        });
    }

}
