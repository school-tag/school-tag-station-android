package org.schooltag.scan;

import com.google.common.base.Optional;
import com.google.common.util.concurrent.FutureCallback;

import org.joda.time.DateTime;
import org.junit.Test;

import javax.annotation.Nullable;

import org.schooltag.FirebaseDatabaseTestBase;
import org.schooltag.entity.EntityNotFoundException;
import org.schooltag.entity.IdFutureCallback;

import static org.awaitility.Awaitility.await;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by aaronroller on 7/16/17.
 */
public class ScanServiceFirebaseTest extends FirebaseDatabaseTestBase {


    @Test
    public void scanCreatedShouldBeFound() {
        final String tagIdValue = "tag-optionalId-1";

        final Scan scan = Scan.builder().tagId(tagIdValue).build();
        IdFutureCallback<Scan, ScanId> pushCallback = new IdFutureCallback<Scan, ScanId>() {
            ScanId scanId;

            @Override
            public void id(ScanId id) {
                assertThat(id, not(equalTo(null)));
                this.scanId = id;
            }

            @Override
            public void onSuccess(@Nullable Object result) {
                assertThat("optionalId should be immediate", scanId, notNullValue());
                bootstrap.scanService.scan(scanId, new FutureCallback<Scan>() {
                    @Override
                    public void onSuccess(@Nullable Scan result) {
                        final Optional<ScanId> optionalId = result.id();
                        assertThat(optionalId.isPresent(), is(true));
                        assertThat("scan came back with a different id", scanId, equalTo(optionalId.get()));
                        assertThat(scan.tagId(), equalTo(result.tagId()));
                        Optional<DateTime> dateTimeOptional = bootstrap.scanService.dateTime(result);
                        assertThat(dateTimeOptional.isPresent(), equalTo(true));
                        final DateTime dateTime = dateTimeOptional.get();
                        assertThat(DateTime.now().minusMinutes(1).isBefore(dateTime), is(true));
                        assertThat(dateTime.getZone(), equalTo(bootstrap.timeZone));
                        testCompletedSuccessfully();
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        testCompleted(t.getMessage());
                    }
                });
            }

            @Override
            public void onFailure(Throwable t) {
                testCompleted(t.getMessage());
            }
        };
        bootstrap.scanService.scanReceived(scan, pushCallback);
    }


    @Test
    public void nonExistingEntityShouldReturnFailure() {
        bootstrap.scanService.scan(new ScanId("junk"), new FutureCallback<Scan>() {
            @Override
            public void onSuccess(@Nullable final Scan result) {
                testCompleted("expecting nothing, but got " + result);
            }

            @Override
            public void onFailure(final Throwable t) {
                assertThat(t, instanceOf(EntityNotFoundException.class));
                testCompletedSuccessfully();
            }
        });
    }


}
