package org.schooltag;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.schooltag.entity.Audit;
import org.schooltag.entity.Path;
import org.schooltag.station.FlavorTest;
import org.schooltag.station.R;

import java.util.concurrent.Callable;

import static java.util.concurrent.TimeUnit.SECONDS;
import static junit.framework.Assert.assertTrue;
import static junit.framework.Assert.fail;
import static org.awaitility.Awaitility.await;

/**
 * Base class to be extended for any firebase tests running against the database.
 * <p>
 * Use the reference as the root of all interactions with the database.  All data is isolated
 * to the single test.
 * <p>
 * Created by aaronroller on 7/20/17.
 */

public class FirebaseDatabaseTestBase {
    protected enum TestType {
        /**
         * isolated testing of the database functions, not needing server side help.
         */
        DATABASE,
        /**
         * Database test reliant on server side triggers to complete actions
         */
        INTEGRATION
    }


    @Rule
    public TestName name = new TestName();
    static Path path;
    private static FirebaseDatabase firebaseDatabase;
    private static DatabaseReference root;
    private DatabaseReference testReference;
    protected SystemBootstrap bootstrap;
    private static String testExecutionId;
    private static Audit auditTemplate;
    final private TestType testType;

    protected FirebaseDatabaseTestBase() {
        this(TestType.DATABASE);
    }

    protected FirebaseDatabaseTestBase(TestType testType) {
        this.testType = testType;
    }


    @BeforeClass
    public static void setupFirebaseTestClass() throws PackageManager.NameNotFoundException {



        //this time is used to create a test path
        //use the developer's time zone, if possible
        final Context applicationContext = FirebaseApp.getInstance().getApplicationContext();
        FlavorTest.assertInTheWild(applicationContext);
        JodaTimeAndroid.init(applicationContext);
        DateTimeZone timeZone = DateTimeZone.getDefault();
        if (timeZone.equals(DateTimeZone.UTC)) {
            //if not detected
            timeZone = DateTimeZone.forOffsetHours(5);
        }
        //make it url friendly
        final DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("YYYY-MM-dd_HH-mm-ss");
        testExecutionId = String.valueOf(DateTime.now().toString(timeFormatter));
        //FIXME:replace user with authenticated user? Maybe an environment variable of developer?

        auditTemplate = SystemBootstrap.androidAuditTemplate(applicationContext);
        path = Path.from("_test")
                .and(auditTemplate.where.replace(".", "_").replace(":", "-"))
                .and(testExecutionId);
    }

    @Before
    public void setupFirebaseTest() {
        //there is a problem initializing this during static runs
        if (firebaseDatabase == null) {
            //set this up once so all tests land in the same test root.
            firebaseDatabase = FirebaseDatabase.getInstance();
            root = firebaseDatabase.getReference().child(path.toString());
            Log.i("FirebaseDatabaseTest", root.toString() + ".json" + " to see test data isolated per test method.");
        }
        final String testName = name.getMethodName();
        testReference = root.child(testName);

        final SystemBootstrap.SystemBootstrapBuilder systemBootstrapBuilder = SystemBootstrap.builder();

        //integration tests use the natural root to take advantage of server triggers
        //other tests are isolated to their own root named after the environment and test name
        final DatabaseReference referenceToUse;
        switch (testType) {
            case INTEGRATION:
                //integration tests use the real root so it can receive cloud logic

                referenceToUse = firebaseDatabase.getReference();
                Log.i(getClass().getSimpleName(), referenceToUse + " for: " + testName);

                break;
            case DATABASE:
                //all data goes to one test root, but each test is isolated
                referenceToUse = testReference;
                break;
            default: throw new IllegalArgumentException(testType.toString() + " not handled");
        }
        systemBootstrapBuilder.databaseReference(referenceToUse);
        bootstrap = systemBootstrapBuilder
                .auditTemplate(auditTemplate)
                //test time zone is the least inhabited place
                .timeZone(DateTimeZone.forOffsetHours(-2)).build();
    }


    //======= Asynchronous Test Wait ==========
    // Currently waits after every test in the after method
    private boolean callbackReceived = false;


    @After
    public void afterTestingWaitForCompletion() {
        waitUntilTestCompletes();
    }

    /**
     * Call this when the asynchronous test completes.
     */
    protected void testCompletedSuccessfully() {
        testCompleted((String) null);
    }

    protected void testCompleted(Throwable throwable) {
        final String message = throwable.getMessage();
        Log.e(getClass().getSimpleName(), message, throwable);
        testCompleted(message);

    }

    /**
     * @param failureMessage
     */
    protected void testCompleted(String failureMessage) {
        if (failureMessage != null) {
            this.callbackReceived = false;
            fail(failureMessage);
        } else {
            this.callbackReceived = true;
        }
    }

    /**
     *
     */
    protected void waitUntilTestCompletes() {
        callbackReceived = false;
        await().atMost(10, SECONDS).until(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return callbackReceived;
            }
        });
    }

}
