package org.schooltag;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;

import lombok.Builder;
import lombok.NonNull;

import org.joda.time.DateTimeZone;
import org.schooltag.entity.Audit;
import org.schooltag.entity.Repository;
import org.schooltag.entity.RepositoryUtils;
import org.schooltag.game.GameService;
import org.schooltag.game.UserRewardsRepository;
import org.schooltag.scan.Scan;
import org.schooltag.scan.ScanRepository;
import org.schooltag.scan.ScanService;
import org.schooltag.tag.TagRepository;
import org.schooltag.tag.TagService;
import org.schooltag.user.UserRepository;
import org.schooltag.user.UserService;

/**
 * Manages singletons and services
 * providing access to connected services.
 * <p>
 * Created by aaronroller on 7/16/17.
 */

public class SystemBootstrap {

    public final UserRepository userRepository;
    public final UserService userService;
    public final ScanRepository scanRepository;
    public final UserRewardsRepository userRewardsRepository;
    public final ScanService scanService;
    public final GameService gameService;
    public final RepositoryUtils repositoryUtils;
    public final DateTimeZone timeZone;
    private final TagRepository tagRepository;
    private final TagService tagService;
    private final FirebaseAuth firebaseAuth;

    @Builder
    SystemBootstrap(final @NonNull DatabaseReference databaseReference,
                    final @NonNull DateTimeZone timeZone,
                    final @NonNull Audit auditTemplate,
                    final @Nullable FirebaseAuth firebaseAuth) {


        this.timeZone = timeZone;
        this.firebaseAuth = firebaseAuth;
        this.repositoryUtils = RepositoryUtils.builder().timeZone(timeZone).build();
        // general repository for each implementation to provide the specific entity handled
        final Repository repositoryTemplate = Repository.builder()
                .firebaseAuth(firebaseAuth)
                .auditTemplate(auditTemplate)
                .root(databaseReference)
                .utils(repositoryUtils).build();

        this.userRepository = UserRepository.builder().repositoryTemplate(repositoryTemplate).build();
        this.userService = UserService.builder().userRepository(userRepository).build();
        this.scanRepository = new ScanRepository(repositoryTemplate);
        this.scanService = new ScanService(scanRepository);

        this.userRewardsRepository = new UserRewardsRepository(repositoryTemplate);

        this.tagRepository = new TagRepository(repositoryTemplate);
        this.tagService = new TagService(tagRepository);

        this.gameService = new GameService(userRewardsRepository, userService, scanService, tagService, repositoryUtils);

    }

    public static Audit androidAuditTemplate(Context applicationContext) {
        String version;
        try {
            final PackageInfo packageInfo = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0);
            version = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(SystemBootstrap.class.getSimpleName(), "unable to determine version", e);
            version = "unknown";
        }
        return Audit.builder()
                .who(Build.USER)
                .where("Android:"
                        + Build.MANUFACTURER + ":"
                        + Build.MODEL + ":"
                        + Build.VERSION.RELEASE)
                .which(version)
                .build();

        //which should get the version of the software
    }
}
