package org.schooltag.entity;

import android.content.pm.PathPermission;
import android.icu.util.TimeZone;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.collect.Lists;
import com.google.firebase.database.DatabaseReference;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.chrono.ISOChronology;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.LinkedList;

import lombok.Builder;

/**
 * Created by aaronroller on 7/16/17.
 */
public  class RepositoryUtils {

    final DateTimeFormatter dateTimeFormatter;
    final ISOChronology chronology;

    /**
     * @param timeZone optionally provided will use the time zone to format
     */
    @Builder
    public RepositoryUtils(@Nullable final DateTimeZone timeZone) {
        //this could be provided, but why?
        this.chronology = ISOChronology.getInstance(timeZone);
        this.dateTimeFormatter = ISODateTimeFormat.dateTimeNoMillis().withChronology(chronology);
    }



    @NonNull
    private static DateTime iso(DateTime dateTime) {
        return dateTime.withChronology(ISOChronology.getInstance());
    }



    public Path path(DatabaseReference reference) {
        //a FIFO queue pushing the children towards the end
        LinkedList<String> elements = Lists.newLinkedList();
        String key;
        while ((key = reference.getKey()) != null) {
            elements.push(key);
            reference = reference.getParent();
        }

        //now build path from parent down.
        Path path = Path.from();
        for (String element : elements) {
            path = path.and(element);
        }
        return path;
    }


    /**
     * @param isoString
     * @return DateTime in the zone as provided
     */
    public DateTime parseDateTime(String isoString) {
        return this.dateTimeFormatter.parseDateTime(isoString);
    }

    /**
     * @param dateTime
     * @return the standard formatted date time with system time zone.
     */
    @lombok.NonNull
    public String portable(@lombok.NonNull final DateTime dateTime) {
        return this.dateTimeFormatter.print(dateTime);
    }

    public DateTime now() {
        return DateTime.now(chronology);
    }

    public DateTime normalized(DateTime dateTime) {
        return dateTime.withChronology(chronology);
    }

    /**
     *
     * @param root the root of the database where all data starts
     * @param pathToEntity the absolute or relative path to the entity from the current root
     * @param rootToInsert the new root, typically the plural name of the parallel entity
     * @return the reference to the parallel entity.
     */
    public DatabaseReference parallelEntityRef(DatabaseReference root, Path pathToEntity,Path rootToInsert){
        final String relativeEntityPathValue = pathToEntity.toString().replace(root.toString(), "");
        return root.child(rootToInsert.toString()).child(relativeEntityPathValue);
    }
}
