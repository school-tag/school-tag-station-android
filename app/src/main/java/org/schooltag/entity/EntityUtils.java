package org.schooltag.entity;

import android.support.annotation.Nullable;
import android.util.Pair;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.base.Supplier;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.schooltag.lang.GenericUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Lombok;
import lombok.NonNull;

import static org.schooltag.lang.GenericUtils.Parameter.FIRST;
import static org.schooltag.lang.GenericUtils.Parameter.SECOND;

/**
 * Created by aaronroller on 7/16/17.
 */

public class EntityUtils {

    /**
     * @param optional
     * @return the string or null
     */
    @Nullable
    public static String optionalToString(Optional<?> optional) {
        return optional.isPresent() ? optional.get().toString() : null;
    }

    public static <E extends Entity<E, I>, I extends Id<E>> Class<I> idType(Class<E> entityClass) {
        return GenericUtils.typeFromParentParameter(entityClass, SECOND);
    }

    public static <E extends Entity<E, I>, I extends Id<E>> Class<E> entityType(Class<E> entityClass) {
        return GenericUtils.typeFromParentParameter(entityClass, FIRST);

    }

    public static <E extends Entity<E, I>, I extends Id<E>> Optional<I> iOptional(String id, Class<E> entityClass) {
        return id != null ? Optional.of(newId(id, entityClass)) : Optional.<I>absent();
    }

    public static <E extends Entity<E, I>, I extends Id<E>> I newId(String id, Class<E> entityClass) {
        final Class[] idRequiredParameterTypes = {String.class};
        try {
            return idType(entityClass).getDeclaredConstructor(idRequiredParameterTypes).newInstance(id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @AllArgsConstructor
    static class EntityIdPair<E extends Entity<E, I>, I extends Id<E>> {
        @NonNull
        public final E entity;
        @NonNull
        public final I id;
    }

    public static <E extends Entity<E, I>, I extends Id<E>> EntityIdPair<E, I> pair(E entity, I id) {
        return new EntityIdPair<>(entity, id);
    }

    /**
     * When you need a function that will use the Builder methods to assign the standard
     * id of an entity.
     *
     * @param entityType
     * @param <E>
     * @param <I>
     * @return
     * @throws NoSuchMethodException
     */
    static <E extends Entity<E, I>, I extends Id<E>> Function<EntityIdPair<E, I>, E> idAssignmentFunction(
            final Class<E> entityType) throws NoSuchMethodException {
        //toBuilder is the standard method name, no constants available
        final Method toBuilder = entityType.getDeclaredMethod("toBuilder");

        return new Function<EntityIdPair<E, I>, E>() {
            @Nullable
            @Override
            public E apply(final EntityIdPair<E, I> input) {
                final E e = input.entity;
                final I i = input.id;
                try {
                    Object builder = toBuilder.invoke(e);
                    builder.getClass().getDeclaredMethod(Entity.ID_FIELD, String.class).invoke(builder, i.toString());
                    //build is the standard method, no constant available
                    E eWithI = (E) builder.getClass().getDeclaredMethod("build").invoke(builder);
                    return eWithI;
                } catch (IllegalAccessException e1) {
                    throw new RuntimeException(e1);
                } catch (InvocationTargetException e1) {
                    throw new RuntimeException(e1);
                } catch (NoSuchMethodException e1) {
                    throw new RuntimeException(e1);
                }
            }
        };


    }

}
