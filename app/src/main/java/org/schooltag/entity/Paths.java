package org.schooltag.entity;

import org.joda.time.DateTime;

/**
 * Paths for common features not specific to any subject.
 *
 * Created by aaronroller on 7/22/17.
 */

public class Paths {

    /**
     * Reference to the day of the week of the year.
     *
     * @param dateTime
     * @return
     */
    public static Path day(DateTime dateTime) {
        return week(dateTime)
                .and("days").and(dateTime.dayOfWeek().getAsString());
    }

    /**
     * Reference to the week of the year.
     *
     * @param dateTime
     * @return
     */
    public static Path week(final DateTime dateTime) {
        return year(dateTime)
                .and("weeks").and(dateTime.weekOfWeekyear().getAsString());
    }

    public static Path year(final DateTime dateTime) {
        return Path.from().and("years").and(dateTime.year().getAsString());
    }
}
