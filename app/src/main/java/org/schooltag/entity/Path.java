package org.schooltag.entity;

import android.support.annotation.VisibleForTesting;

import lombok.NonNull;

/**
 * Created by aaronroller on 7/21/17.
 */
public class Path {

    @VisibleForTesting
    static final String DELIMITER = "/";
    private final String path;

    protected Path(String pathOrElement) {
        this.path = this.prefix(trim(pathOrElement));
    }

    protected Path(Path path) {
        this(path.toString());
    }

    protected Path() {
        this("");
    }

    private String append(final String pathOrElement) {
        return this.path + prefix(this.trim(pathOrElement));
    }

    public static Path from(String path) {
        return new Path(path);
    }


    public static Path from() {
        return new Path();
    }

    public Path and(@NonNull Object pathOrElement) {
        return new Path(append(pathOrElement.toString()));
    }

    @android.support.annotation.NonNull
    private String prefix(final String value) {
        final String result;
        if (value.startsWith(DELIMITER) || (this.path != null && this.path.endsWith(DELIMITER))) {
            result = value;
        } else {
            result = DELIMITER + value;
        }
        return result;
    }

    @android.support.annotation.NonNull
    private String trim(String result) {
        //ensure the new path doesn't end with /
        while (result.endsWith(DELIMITER) && !result.equals(DELIMITER)) {
            result = result.substring(0, result.length() - 1);
        }
        return result;
    }

    @Override
    public String toString() {
        return path;
    }
}
