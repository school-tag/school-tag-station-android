package org.schooltag.entity;

import org.schooltag.entity.EntityUtils.EntityIdPair;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Pair;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.common.base.Function;
import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.FutureCallback;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import org.joda.time.DateTime;
import org.schooltag.station.R;
import org.schooltag.lang.GenericUtils;

import java.util.List;

import static org.schooltag.lang.GenericUtils.Parameter.FIRST;
import static org.schooltag.lang.GenericUtils.Parameter.SECOND;

/**
 * Base class for persisting all Entities.
 * <p>
 * Created by aaronroller on 7/15/17.
 *
 * @param <E> entity this manages
 * @param <I> identifies E
 */
public class Repository<E extends Entity<E, I>, I extends Id<E>> {
    /**
     * The top level of the database .
     */
    @Getter
    protected final DatabaseReference root;
    @Getter
    private final RepositoryUtils utils;
    private final Audit auditTemplate;

    private final Class<E> entityType;
    private final FirebaseAuth firebaseAuth;
    final Function<EntityIdPair<E, I>, E> idAssignmentFunction;

    /**
     * use builder
     *
     * @param root
     * @param utils
     */
    @Deprecated
    public Repository(DatabaseReference root, RepositoryUtils utils) {
        this(root, utils, null, Audit.builder().who("unknown").where("TBD").build(), null, null);
    }

    @Builder(toBuilder = true)
    public Repository(DatabaseReference root,
                      @lombok.NonNull final RepositoryUtils utils,
                      @Nullable final Class<E> entityType,
                      @lombok.NonNull final Audit auditTemplate,
                      @Nullable final FirebaseAuth firebaseAuth,
                      @Nullable Function<EntityIdPair<E, I>, E> idAssignmentFunction) {
        this.root = root;
        this.utils = utils;
        this.entityType = entityType;
        this.auditTemplate = auditTemplate;
        this.firebaseAuth = firebaseAuth;
        if (idAssignmentFunction != null) {
            this.idAssignmentFunction = idAssignmentFunction;
        } else if (this.entityType != null) {
            try {
                this.idAssignmentFunction = EntityUtils.idAssignmentFunction(this.entityType);
            } catch (NoSuchMethodException e) {
                throw new RuntimeException("Either provide an id assigment function or put a toBuilder=true into your Builder annotation on Entity.");
            }
        } else {
            this.idAssignmentFunction = null;
        }

    }

    public void push(E entity, final FutureCallback<I> callback) {
        push(entity, this.root, callback);
    }

    /**
     * Persists an entity, optionally providing the optionalId if {@link IdFutureCallback} is provided.
     *
     * @param entity
     * @param entitiesRef the location where the entity is to be persisted
     * @param callback    communicates the results when ready. Id is provided immediately if {@link IdFutureCallback} is provided
     */
    public void push(E entity, DatabaseReference entitiesRef, final FutureCallback<I> callback) {
        final DatabaseReference push = entitiesRef.push();
        final String idValue = push.getKey();
        final Path path = utils.path(entitiesRef).and(idValue);
        final I id = EntityUtils.newId(path.toString(), entityType());
        if (callback instanceof IdFutureCallback) {
            ((IdFutureCallback<E, I>) callback).id(id);
        }

        final Task<Void> task = push.setValue(entity);

        //set the newly created id with the entity for convenience when loading
        task.addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull final Task<Void> task) {
                if(task.isSuccessful()){
                    push.child(Entity.ID_FIELD).setValue(idValue);
                }
            }
        });
        task.addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callback.onSuccess(id);
                } else {
                    callback.onFailure(task.getException());
                }
            }
        });

        //record the audit

        final Audit.AuditBuilder auditBuilder = this.auditTemplate.toBuilder();
        if (this.firebaseAuth != null) {
            final FirebaseUser currentUser = this.firebaseAuth.getCurrentUser();
            if (currentUser != null) {
                auditBuilder.who(currentUser.getUid());
            }
        }
        final Audit audit = auditBuilder
                .what("push")
                .when(this.utils.portable(this.utils.now())).build();
        final DatabaseReference auditRef = this.utils.parallelEntityRef(this.root, path, Path.from("audits"));
        //no need to report success/failure
        auditRef.push().setValue(audit);
    }


    /**
     * Retrieves the entity at the default location for the repository.
     *
     * @param id       of the entity
     * @param callback notifies of success and failure.
     */
    public void get(final I id, final FutureCallback<E> callback) {
        get(id, this.root, callback);
    }


    /**
     * Retrieves the entity found at the ID in the standard location for the repository.
     *
     * @param id       of the entity
     * @param callback success provides the entity, failure provides the exception
     */
    public void get(final I id, DatabaseReference databaseReference,
                    final FutureCallback<E> callback) {
        final ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null && dataSnapshot.hasChildren()) {
                    final E entityWithId = entityFromSnapshot(id, dataSnapshot);
                    callback.onSuccess(entityWithId);
                } else {
                    callback.onFailure(EntityNotFoundException.of(R.string.error_entity_not_found, id));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.onFailure(databaseError.toException());
            }
        };
        //ids have the absolute reference
        databaseReference.getRoot().child(id.toString()).addValueEventListener(valueEventListener);
    }

    /**
     * Provides an ordered list of entities found at the path given.
     *
     * @param path     the location of the entities.
     * @param callback provides the list or a reason the list is not available
     */
    public void list(Path path, final FutureCallback<List<E>> callback) {
        this.root.child(path.toString()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {
                ImmutableList.Builder<E> builder = ImmutableList.builder();
                if (dataSnapshot.hasChildren()) {
                    for (DataSnapshot childSnapshot :
                            dataSnapshot.getChildren()) {
                        final Path childPath = utils.path(childSnapshot.getRef());
                        final I id = EntityUtils.newId(childPath.toString(), entityType());
                        builder.add(entityFromSnapshot(id, childSnapshot));
                    }
                }
                callback.onSuccess(builder.build());
            }

            @Override
            public void onCancelled(final DatabaseError databaseError) {
                callback.onFailure(databaseError.toException());
            }
        });
    }

    /**
     * provides an entity from the snapshot, typically a callback.
     * Some repositories may expand elaborate the value of the ID by overriding this.
     *
     * @param id
     * @param dataSnapshot
     * @return
     */
    protected E entityFromSnapshot(final I id,
                                   final DataSnapshot dataSnapshot) {
        E entity = dataSnapshot.getValue(entityType());
        if (this.idAssignmentFunction != null) {
            entity = this.idAssignmentFunction.apply(EntityUtils.pair(entity, id));
        }
        return entity;
    }



    /**
     * @return the class for the optionalId of the entity that this repository manages
     */
    protected Class<I> idType() {
        return GenericUtils.typeFromParentParameter(entityType(), SECOND);
    }

    /**
     * @return class representing the entity this repository manages
     */
    final protected Class<E> entityType() {
        if (entityType != null) {
            return entityType;
        } else {
            return GenericUtils.typeFromParentParameter(getClass(), FIRST);
        }
    }


}
