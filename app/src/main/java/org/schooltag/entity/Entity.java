package org.schooltag.entity;

import android.support.annotation.Nullable;

import com.google.common.base.Optional;

import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Base class for all persisted model objects with identity.
 * Created by aaronroller on 7/15/17.
 *
 * @param <E> the type of entity for this
 */
@NoArgsConstructor(force = true)
@ToString(of = "id")
abstract public class Entity<E extends Entity<E, I>, I extends Id<E>> {
    static final String ID_FIELD = "id";
    /**
     * the value of the ID. public and primitive for Firebase.
     *
     * @see #id()
     */
    @Nullable
    public final String id;

    protected Entity(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Entity<?, ?> entity = (Entity<?, ?>) o;

        return id != null ? id.equals(entity.id) : entity.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    /**
     * Entities may be created without being identified yet.
     *
     * @return optionalId if available
     */
    public Optional<I> id() {
        return EntityUtils.iOptional(this.id, getClass());
    }
}
