package org.schooltag.entity;

import org.schooltag.lang.ResourceException;

/**
 * Created by aaronroller on 7/22/17.
 */

public class EntityNotFoundException extends ResourceException {
    public EntityNotFoundException(int errorCode, Id id, Throwable throwable) {
        super(errorCode, "No Entity found matching " + id, throwable);
    }

    public static EntityNotFoundException of(int errorCode, Id id) {
        return new EntityNotFoundException(errorCode, id, null);
    }
}
