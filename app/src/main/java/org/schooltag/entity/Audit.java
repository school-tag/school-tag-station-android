package org.schooltag.entity;

import lombok.Builder;

/**
 * Identifies who, what, where, when, which and why an entity was modified.
 *
 * Created by aaronroller on 7/23/17.
 */
@Builder(toBuilder = true)
public class Audit {
    /**
     * the user logged into the app.
     */
    public final String who;
    /**
     * What was the modification?  Create, save, etc.
     */
    public final String what;
    /**
     * The type of device, os, etc.  The User Agent.
     */
    public final String where;
    /**
     * the time of modification
     */
    public final String when;
    /**
     * the version of the application
     */
    public final String which;
    /**
     * an action code indicating why the entity was modified.
     */
    public final String why;

}
