package org.schooltag.entity;

import com.google.common.util.concurrent.FutureCallback;

/**
 * FutureCallback with an immediate ID available.
 * Created by aaronroller on 7/17/17.
 */
public interface IdFutureCallback<E extends Entity<E, I>, I extends Id<E>> extends FutureCallback {
    /**
     * Immediately available, this is the optionalId being assigned to an entity.
     */
    void id(I id);
}
