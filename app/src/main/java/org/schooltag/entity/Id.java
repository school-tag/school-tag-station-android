package org.schooltag.entity;

import java.lang.reflect.ParameterizedType;
import java.util.Arrays;

import lombok.NonNull;

/**
 * Base class for all identifiers that identify an entity.
 * Created by aaronroller on 7/15/17.
 *
 * @param <E> the entity this identifies
 */
abstract public class Id<E> {
    static final String VALUE = "value";
    @NonNull
    private final String value;

    public Id(@NonNull final String value) {
        this.value = value;

    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Id<?> id = (Id<?>) o;
        //confirm this identifies the same entity type
        if (!identifiesType().equals(((Id<?>) o).identifiesType())) {
            return false;
        }
        return value.equals(id.value);
    }

    /**
     *
     * @return the entity type this identifies
     */
    public Class<E> identifiesType() {
        return (Class) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    /**Provides ability for implementation to easily share the key of the entity,
     * rather than the entire path.
     *
     * @return the last path element, which is assumed to be the main key.
     */
    protected String key(){
        final String[] elements = value.split("/");
        return elements[elements.length-1];
    }
}
