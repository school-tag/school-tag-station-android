package org.schooltag.tag;

import lombok.NonNull;

import org.schooltag.entity.Id;
import org.schooltag.entity.Path;

/**
 * Created by aaronroller on 7/16/17.
 */

public class TagId extends Id<Tag> {
    public TagId(@NonNull String value) {
        super(value);
    }

    public static final TagId fromKey(String key) {
        return new TagId(Path.from("tags").and(key).toString());
    }
}
