package org.schooltag.tag;

import com.google.common.base.Strings;
import com.google.common.util.concurrent.FutureCallback;
import com.google.firebase.database.DatabaseReference;

import org.schooltag.entity.Entity;
import org.schooltag.entity.Id;
import org.schooltag.entity.Repository;
import org.schooltag.entity.RepositoryUtils;
import org.schooltag.user.UserId;

import lombok.Builder;
import lombok.NonNull;

/** Persistence for {@link Tag}.
 *
 * Created by aaronroller on 7/20/17.
 */
public class TagRepository {

    private final Repository<Tag, TagId> repository;

    public TagRepository(Repository<Tag, TagId> repository) {
        this.repository = repository.toBuilder().entityType(Tag.class).build();
    }

    public void get(final TagId tagId, final FutureCallback<Tag> tagFutureCallback) {
        repository.get(tagId, tagFutureCallback);
    }
}
