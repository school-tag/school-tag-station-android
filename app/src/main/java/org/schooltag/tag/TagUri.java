package org.schooltag.tag;

import android.net.Uri;

import java.util.List;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;

/**
 * Decodes the uri into parts to pass on during the scan.
 *
 * Created by aaronroller on 1/3/18.
 */
@Builder
public class TagUri {

    public final String category;
    public final String type;
    public final String host;

    /**
     *
     * @param uri
     * @return the completed TagUri
     * @throws IllegalArgumentException when provided an unhandled URI.
     */
    public static TagUri valueOf(Uri uri) {

        final TagUriBuilder builder = TagUri.builder();
        builder.host(uri.getHost());
        final List<String> pathSegments = uri.getPathSegments();
        if (pathSegments.size() == 3 && "scan".equals(pathSegments.get(0))) {
            builder.category(pathSegments.get(1));
            builder.type(pathSegments.get(2));
        } else {
            throw new IllegalArgumentException("Not a valid uri: " + uri.toString());
        }
        return builder.build();
    }
}
