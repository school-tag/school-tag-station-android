package org.schooltag.tag;

import com.google.common.util.concurrent.FutureCallback;

import lombok.AllArgsConstructor;
import lombok.Builder;

/**
 * Provides access to information about a {@link Tag},
 * like the user that owns it.
 * <p>
 * Created by aaronroller on 7/21/17.
 */

@AllArgsConstructor
@Builder
public class TagService {
    private TagRepository tagRepository;


    public void tag(TagId tagId, FutureCallback<Tag> tagFutureCallback) {
        tagRepository.get(tagId, tagFutureCallback);
    }
}
