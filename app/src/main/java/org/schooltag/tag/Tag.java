package org.schooltag.tag;

import com.google.common.base.Optional;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import org.schooltag.entity.Entity;
import org.schooltag.user.UserId;

/**
 * Created by aaronroller on 7/9/17.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class Tag extends Entity<Tag, TagId> {
    public final String userId;

    @Builder(toBuilder = true)
    protected Tag(String id, String userId) {
        super(id);
        this.userId = userId;
    }

    public Optional<UserId> userId() {
        return (userId != null) ? Optional.of(UserId.fromKey(this.userId)) : Optional.<UserId>absent();
    }


    public static String formatTagId(final String tagId){
        return (tagId != null) ? tagId.toUpperCase() : null;
    }
}
