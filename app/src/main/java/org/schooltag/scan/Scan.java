package org.schooltag.scan;

import android.support.annotation.Nullable;

import com.google.common.base.Optional;

import org.joda.time.DateTime;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.schooltag.entity.Entity;
import org.schooltag.entity.EntityUtils;
import org.schooltag.entity.Id;
import org.schooltag.entity.RepositoryUtils;
import org.schooltag.tag.Tag;
import org.schooltag.tag.TagId;

/**
 * Created by aaronroller on 7/9/17.
 */
@NoArgsConstructor(force = true)
public class Scan extends Entity<Scan, ScanId> {

    public static final String CATEGORY_BUS = "bus";
    public static final String CATEGORY_BACKPACK = "backpack";
    public static final String CATEGORY_WALK = "walk";
    public static final String TAG_TYPE_CLIPPER = "clipper";
    @NonNull
    public final String tagId;
    @NonNull
    public final String timestamp;
    /**The authenticated user's Firebase UID */
    public final String authUid;

    /** If provided, the given tag will be associated with the user id.*/
    public final String userIdOfTagOwner;
    /** If provided, the scan will reward the user identified since the tag is not associated with
     * a user.*/
    public final String userIdOfRewardRecipient;
    /**
     * Rewards the player with special achievements like bus, walk.
     */
    @Nullable
    public final String category;
    @Nullable
    /**
     * Communicates extra knowledge about the tag being read.
     * - Tag = school tag provided NFC
     * - qrc = school tag providd Quick Read Code
     * - clipper = Clipper Transit Card
     */
    public final String tagType;
    /**
     * Indicates when a tag has a special category (walk station, bus station, etc).
     * This is not used for the School Tag Station, but may be communicated when the url is parsed
     * for debugging purposes.
     */
    public final String tagCategory;

    @Builder(toBuilder = true)
    private Scan(@Nullable final String id,
                 @Nullable final String tagId,
                 @Nullable final String authUid,
                 @Nullable final String timestamp,
                 @Nullable final String category,
                 @Nullable final String tagCategory,
                 @Nullable final String tagType,
                 @Nullable final String userIdOfRewardRecipient,
                 @Nullable final String userIdOfTagOwner) {
        super(id);
        this.tagId = Tag.formatTagId(tagId);
        this.timestamp = timestamp;
        this.authUid = authUid;
        this.category = category;
        this.tagType = tagType;
        this.tagCategory = tagCategory;
        this.userIdOfTagOwner = userIdOfTagOwner;
        this.userIdOfRewardRecipient = userIdOfRewardRecipient;
    }


    public Optional<TagId> tagId() {
        return this.tagId != null ? Optional.of(TagId.fromKey(tagId)) : Optional.<TagId>absent();
    }

    @Override
    public Optional<ScanId> id() {
        return id != null ? Optional.of(new ScanId(id)) : Optional.<ScanId>absent();
    }


}
