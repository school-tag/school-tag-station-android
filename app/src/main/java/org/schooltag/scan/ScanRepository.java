package org.schooltag.scan;

import android.support.annotation.Nullable;
import android.util.Pair;

import com.google.common.base.Function;
import com.google.common.util.concurrent.FutureCallback;
import com.google.firebase.database.DatabaseReference;

import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import org.schooltag.entity.Audit;
import org.schooltag.entity.EntityUtils;
import org.schooltag.entity.Repository;
import org.schooltag.entity.RepositoryUtils;
import org.schooltag.tag.TagId;

import lombok.Builder;
import lombok.NonNull;

/**
 * Manages persistence related to all scans.
 * Created by aaronroller on 7/15/17.
 */
public class ScanRepository {

    private final Repository<Scan, ScanId> repository;
    private final DatabaseReference scansRef;

    @Builder
    public ScanRepository(Repository repositoryTemplate) {
        repository = repositoryTemplate.toBuilder().entityType(Scan.class).build();
        this.scansRef = this.repository.getRoot().child("scans");
    }

    void push(Scan scan, final FutureCallback<ScanId> callback) {
        repository.push(scan, scansRef, callback);
    }

    public void get(ScanId scanId, final FutureCallback<Scan> callback) {
        repository.get(scanId, callback);
    }

    public RepositoryUtils getUtils() {
        return this.repository.getUtils();
    }
}
