package org.schooltag.scan;

import lombok.NonNull;
import org.schooltag.entity.Id;

/**
 * Created by aaronroller on 7/16/17.
 */

public class ScanId extends Id<Scan> {
    public ScanId(@NonNull String value) {
        super(value);
    }

    public String scanKey() {
        return super.key();
    }
}
