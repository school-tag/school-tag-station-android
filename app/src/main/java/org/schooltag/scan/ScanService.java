package org.schooltag.scan;

import android.support.annotation.VisibleForTesting;
import android.util.Pair;

import com.google.android.gms.tasks.Task;
import com.google.common.base.Optional;
import com.google.common.util.concurrent.FutureCallback;

import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.schooltag.entity.RepositoryUtils;

import java.util.Scanner;

import lombok.Builder;

/**
 * Created by aaronroller on 7/16/17.
 */
public class ScanService {
    private final ScanRepository scanRepository;
    private final RepositoryUtils repositoryUtils;

    @Builder
    public ScanService(ScanRepository repository) {

        this.scanRepository = repository;
        this.repositoryUtils = this.scanRepository.getUtils();
    }

    /**
     * Creates a scan and returns the new optionalId with a callback to handle results.
     * <p>
     * Now will be assigned the timestamp if not already provided.
     *
     * @param scan     containing the recently scanned {@link org.schooltag.tag.Tag}
     * @param callback notifying success and failure. If
     *                 {@link org.schooltag.entity.IdFutureCallback} provided then ID will
     *                 be given immediately
     * @return
     */
    public void scanReceived(Scan scan, FutureCallback<ScanId> callback) {
        scan = scanWithTimestamp(scan);
        this.scanRepository.push(scan, callback);
    }

    @VisibleForTesting
    Scan scanWithTimestamp(Scan scan) {
        final String now = this.repositoryUtils.portable((this.repositoryUtils.now()));
        scan = scan.toBuilder().timestamp(now).build();
        return scan;
    }

    /**
     * Gets the scan identified by the ID.
     */
    public void scan(ScanId scanId, FutureCallback<Scan> callback) {
        this.scanRepository.get(scanId, callback);
    }

    /**
     * @param scan containing timestamp
     * @return the optional datetime parsed from the timestamp in the scan
     */
    public Optional<DateTime> dateTime(Scan scan) {
        DateTime dateTime = null;
        if (scan.timestamp != null) {
            dateTime = this.repositoryUtils.parseDateTime(scan.timestamp);
        }
        return Optional.fromNullable(dateTime);
    }
}
