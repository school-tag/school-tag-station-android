package org.schooltag.station;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.common.base.Optional;
import com.google.common.util.concurrent.FutureCallback;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.FirebaseDatabase;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTimeZone;
import org.schooltag.SystemBootstrap;
import org.schooltag.entity.Audit;
import org.schooltag.game.Reward;
import org.schooltag.game.ScanReward;
import org.schooltag.game.UserRewards;
import org.schooltag.nfc.NfcUtils;
import org.schooltag.scan.Scan;
import org.schooltag.tag.TagUri;
import org.schooltag.user.User;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private static final int RC_SIGN_IN = 9001;

    private SystemBootstrap bootstrap;

    private FirebaseAuth mAuth;
    private GoogleApiClient mGoogleApiClient;

    /**
     * Available for viewing the user's daily stats.
     *
     * @see #infoCanBeShown(Scan)
     */
    private Optional<User> userMostRecentlyScanned = Optional.absent();

    /**
     * Only a single scan can be shown at a time. This is used to ensure recipt of stale data
     * is not shown if the next scan has happened.
     */
    private Optional<Scan> scanCurrentlyShown = Optional.absent();

    /**
     * If a dialog is currently shown, then the next scan must associate the tag with the current user.
     */
    private Optional<AlertDialog> associateScanToCurrentUserDialog = Optional.absent();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        JodaTimeAndroid.init(this);


        //Authentication setup
        signInButton().setOnClickListener(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // [END config_signin]

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mAuth = FirebaseAuth.getInstance();

        Audit auditTemplate = SystemBootstrap.androidAuditTemplate(getApplicationContext());
        this.bootstrap = SystemBootstrap.builder()
                .databaseReference(FirebaseDatabase.getInstance().getReference())
                .timeZone(DateTimeZone.getDefault())
                .auditTemplate(auditTemplate)
                .firebaseAuth(this.mAuth).build();
        resetView();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        //see onResume
    }

    @Override
    protected void onResume() {
        super.onResume();
        //https://stackoverflow.com/a/36942185 provided the insight for handling this way
        //always called after onNewIntent and onCreate allowing a tag reward for a closed app
        handleNfcIntent(getIntent());
        //clear the intent once it is handled so it won't come back

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Reads the NFC tag id, creates a scan and listens for all the changes that may occur from the
     * scan being received (rewards, etc.)
     *
     * @param intent
     */
    private void handleNfcIntent(@Nullable Intent intent) {
        if (isSupportedIntent(intent)) {
            final FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser != null) {
                //the old view needs to be cleared to make room for the new
                resetView();
                //new intent being handled.  clear out the old one
                getIntent().setAction("");
                Log.d("0xk8", "handling tag" + intent.getAction());

                //the id is all we need
                byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
                final String hexId = NfcUtils.toReversedHex(id);
                final Scan.ScanBuilder scanBuilder = Scan.builder();
                //uri is the url for schooltag
                Uri uri = intent.getData();
                if (uri != null) {
                    Log.d("xx92", "tag uri is: " + uri.toString());
                    TagUri tagUri = TagUri.valueOf(uri);
                    scanBuilder.tagType(tagUri.type);
                    scanBuilder.tagCategory(tagUri.category);
                } else {
                    //not a school tag NFC...probably a clipper card?
                    Toast.makeText(this, "Tag is not a School Tag", Toast.LENGTH_LONG).show();
                    //FIXME: don't just assume it is a clipper, read the payload
                    scanBuilder.tagType(Scan.TAG_TYPE_CLIPPER);
                    scanBuilder.tagCategory(Scan.CATEGORY_BACKPACK);

                }

                //the presence of the associate scan dialog means we are associating this tag to the user
                if (associateScanToCurrentUserDialog.isPresent()) {
                    scanBuilder.userIdOfRewardRecipient(userMostRecentlyScanned.get().id().get().toKey());
                    nextScanIsNotAssociated();
                }

                final Scan scan = scanBuilder
                        .authUid(currentUser.getUid())
                        .tagId(hexId).build();
                //show the spinner
                calculatingPointsProgressView().setVisibility(View.VISIBLE);
                instructionsDisplay().setVisibility(View.INVISIBLE);

                this.scanCurrentlyShown = Optional.of(scan);
                hideSoon(scan);

                //FIXME: de-register the listner?
                bootstrap.gameService.rewardsForScanReceived(scan,
                        todayScanRewardsCallback(scan),
                        todayUserRewardsCallback(scan),
                        null,
                        yearUserRewardsCallback(scan),
                        userCallback(scan));

                offlineCheck(scan);

            } else {
                Toast.makeText(this, getString(R.string.sign_in_required_for_scan_message), Toast.LENGTH_LONG).show();
            }
        }

    }

    @NonNull
    private FutureCallback<User> userCallback(final Scan scan) {
        return new FutureCallback<User>() {
            @Override
            public void onSuccess(final User result) {
                if (infoCanBeShown(scan)) {
                    showingUserResults(result);
                }
            }

            @Override
            public void onFailure(final Throwable t) {
                showMessage(R.string.error_user_not_found);
                Log.w("2lx4", t.getMessage());
            }
        };
    }

    @NonNull
    private FutureCallback<List<ScanReward>> todayScanRewardsCallback(final Scan scan) {
        return new FutureCallback<List<ScanReward>>() {
            @Override
            public void onSuccess(final List<ScanReward> result) {
                if (infoCanBeShown(scan)) {
                    showTodaysScanRewards(result);
                }
            }

            @Override
            public void onFailure(final Throwable t) {
                Log.e("2x90", "Problem listing today rules for scan " + scan.tagId, t);
                //don't show anything since these are just icons
            }
        };
    }

    @NonNull
    private FutureCallback<UserRewards> yearUserRewardsCallback(final Scan scan) {
        return new FutureCallback<UserRewards>() {
            @Override
            public void onSuccess(final UserRewards thisYearRewards) {
                if (infoCanBeShown(scan)) {
                    showYearUserRewards(thisYearRewards);
                }
            }

            @Override
            public void onFailure(final Throwable t) {
                //this is a nice-to-have feature.  don't show anything
                Log.w("1zaq", "unable to get week totals", t);
            }
        };
    }

    @NonNull
    private FutureCallback<UserRewards> todayUserRewardsCallback(final Scan scan) {
        return new FutureCallback<UserRewards>() {
            @Override
            public void onSuccess(final UserRewards todaysUserRewards) {
                if (infoCanBeShown(scan)) {
                    showTodayUserRewards(todaysUserRewards);
                }
            }

            @Override
            public void onFailure(final Throwable t) {
                Log.w("ls93", t.getMessage());
            }
        };
    }

    private void offlineCheck(Scan scan) {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        //if offline, pop up a message and no loading symbol
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        if (!isConnected) {
            resetView();
            Toast.makeText(this, "Offline.  The scan of " + scan.tagId + " will be sent later.", Toast.LENGTH_SHORT).show();
        }
    }

    private void showTodayUserRewards(final UserRewards todaysUserRewards) {
        if (todaysUserRewards != null) {
            //points are being updated on the fly.
            //ignore early triggers and show only the good ones
            if (todaysUserRewards.points != null) {
                final TextView todaysValueText = totalTodayValueView();
                todaysValueText.setText(String.valueOf(todaysUserRewards.points));
                calculatingPointsProgressView().setVisibility(View.INVISIBLE);
                todaysValueText.setVisibility(View.VISIBLE);
            }
            if (todaysUserRewards.coins != null && todaysUserRewards.coins > 0) {
                coinIcon().setVisibility(View.VISIBLE);
            }
        }
    }

    private void showYearUserRewards(final UserRewards thisYearRewards) {
        if (thisYearRewards != null) {
            if (thisYearRewards.points != null) {
                final TextView yearValueText = totalYearValueView();
                yearValueText.setText(String.valueOf(thisYearRewards.points));
                yearValueText.setVisibility(View.VISIBLE);
                yearTotalLabelView().setVisibility(View.VISIBLE);
            }
            if (thisYearRewards.coins != null) {
                final TextView yearCoinValue = totalYearCoinValueView();
                yearCoinValue.setText(String.valueOf(thisYearRewards.coins));
                yearCoinValue.setVisibility(View.VISIBLE);
                totalCoinIcon().setVisibility(View.VISIBLE);
            }
        }
    }

    private void showTodaysScanRewards(List<ScanReward> result) {
        if (result != null && !result.isEmpty()) {
            for (ScanReward scanReward : result) {
                for (Map.Entry<String, Reward> entry : scanReward.rules.entrySet()) {
                    String ruleId = entry.getKey();
                    final View ruleIcon = ruleIcon(ruleId);
                    if (ruleIcon != null) {
                        ruleIcon.setVisibility(View.VISIBLE);
                    } else {
                        Log.w("lns2", "Unable to find icon for "
                                + ruleId);
                    }
                }
            }
        }
    }

    /**
     * @param result the user most recently scanned
     */
    private void showingUserResults(User result) {
        if (result != null) {
            userMostRecentlyScanned = Optional.of(result);
            viewPlayerDetailsButton().setVisibility(View.VISIBLE);
            viewTokenRewardButton().setVisibility(View.VISIBLE);
            if (result.nickname != null) {
                nicknameView().setText(result.nickname);

                if (result.avatar != null) {
                    final String url = "https://school-tag-avatars.imgix.net"
                            + result.avatar
                            + "?auto=format,enhance&h=40&w=40";
                    new BackgroundImageTask().execute(url);
                } else {
                    Log.w("s92k", "avatar not provided");
                }


            } else {
                nicknameView().setText(getString(R.string.total_value_default_text));
            }
        }
    }

    /**
     * Indicates if the most recent scanned user is related to the information being received.
     * <p>
     * Sometimes, slow callbacks are fast scanning can mingle different user's rewards.
     *
     * @param scan with the incoming information
     * @return true if the user given is the same as the most recent scanned user.
     * @see #resetView()
     * @see #showingUserResults(User)
     */
    private boolean infoCanBeShown(final Scan scan) {
        final boolean result;
        if (scanCurrentlyShown.isPresent()) {
            Scan showing = scanCurrentlyShown.get();
            result = showing.tagId().equals(scan.tagId());
        } else {
            result = false;
        }
        return result;
    }


    private boolean isSupportedIntent(final @Nullable Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            return NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)
                    || NfcAdapter.ACTION_TAG_DISCOVERED.equals(action);
        } else {
            return false;
        }
    }

    private View yearTotalLabelView() {
        return findViewById(R.id.total_year_label);
    }

    private View coinIcon() {
        return findViewById(R.id.coin_icon);
    }

    private View totalCoinIcon() {
        return findViewById(R.id.total_coins_icon);
    }

    /**
     * Hides all of the view for the current scan after showing for a brief time long enough
     * for the participant to understand the points.
     *
     * @param scan is the recent scan
     */
    private void hideSoon(final Scan scan) {
        Timer t = new Timer();
        TimerTask hideSoonTask = new TimerTask() {
            @Override
            public void run() {
                //must run in the view thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //only reset the view if other scan has been received from another user
                        if (infoCanBeShown(scan)) {
                            resetView();
                            scanCurrentlyShown = Optional.absent();
                        }
                    }
                });
            }
        };
        t.schedule(hideSoonTask, 8000L);
    }

    private TextView totalYearValueView() {
        return (TextView) findViewById(R.id.total_year_value);
    }

    private TextView totalYearCoinValueView() {
        return (TextView) findViewById(R.id.total_coins_value);
    }

    private FloatingActionButton viewPlayerDetailsButton() {
        return (FloatingActionButton) findViewById(R.id.view_player_details_button);
    }

    private FloatingActionButton viewTokenRewardButton() {
        return (FloatingActionButton) findViewById(R.id.token_reward_button);
    }

    private TextView totalTodayValueView() {
        return (TextView) findViewById(R.id.total_today_value);
    }

    private ProgressBar calculatingPointsProgressView() {
        return (ProgressBar) findViewById(R.id.calculating_points_progress);
    }

    private TextView instructionsDisplay() {
        return (TextView) findViewById(R.id.instructions_text);
    }

    private TextView nicknameView() {
        return (TextView) findViewById(R.id.nickname);
    }

    private ImageView avatarView() {
        return (ImageView) findViewById(R.id.avatar);
    }

    /**
     * clears out the fields
     */
    private void resetView() {
        final String nada = getString(R.string.total_value_default_text);
        final int message = R.string.short_instructions;
        showMessage(message);
        totalTodayValueView().setText(nada);
        totalYearValueView().setText(nada);
        nicknameView().setText(nada);
        calculatingPointsProgressView().setVisibility(View.INVISIBLE);
        yearTotalLabelView().setVisibility(View.INVISIBLE);
        findViewById(R.id.arrivedByBike_icon).setVisibility(View.INVISIBLE);
        findViewById(R.id.arrivedByBus_icon).setVisibility(View.INVISIBLE);
        findViewById(R.id.arrivedByFoot_icon).setVisibility(View.INVISIBLE);
        findViewById(R.id.morningArrivalEarly_icon).setVisibility(View.INVISIBLE);
        findViewById(R.id.morningArrivalOnTime_icon).setVisibility(View.INVISIBLE);
        findViewById(R.id.morningArrivalLate_icon).setVisibility(View.INVISIBLE);
        findViewById(R.id.everyScanCounts_icon).setVisibility(View.INVISIBLE);
        avatarView().setVisibility(View.INVISIBLE);
        coinIcon().setVisibility(View.INVISIBLE);
        totalCoinIcon().setVisibility(View.INVISIBLE);
        totalYearCoinValueView().setVisibility(View.INVISIBLE);
        viewPlayerDetailsButton().setVisibility(View.INVISIBLE);
        viewTokenRewardButton().setVisibility(View.INVISIBLE);

    }

    private void showMessage(final int message) {
        instructionsDisplay().setText(getString(message));
        instructionsDisplay().setVisibility(View.VISIBLE);
        calculatingPointsProgressView().setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // [START on_start_check_user]
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }
    // [END on_start_check_user]

    // [START onactivityresult]
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // [START_EXCLUDE]
                Log.e("knsk", result.getStatus().toString());
                updateUI(null);
                // [END_EXCLUDE]
            }
        }
    }
    // [END onactivityresult]

    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d("0xpo", "firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
        //Toast.makeText(this, "signing in ", Toast.LENGTH_SHORT).show();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {

                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("xx93", "signInWithCredential:success");
                            updateUI(mAuth.getCurrentUser());
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("kl2m", "signInWithCredential:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // [START_EXCLUDE]
                        //Toast.makeText(MainActivity.this, "done signing in ", Toast.LENGTH_SHORT).show();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_google]

    // [START signin]
    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
    // [END signin]

    private void signOut() {
        // Firebase sign out
        mAuth.signOut();

        // Google sign out
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        updateUI(null);
                    }
                });
    }

    private void revokeAccess() {
        // Firebase sign out
        mAuth.signOut();

        // Google revoke access
        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        updateUI(null);
                    }
                });
    }


    private void updateUI(FirebaseUser user) {
        if (user != null) {
//            final String message = getString(R.string.sign_in_success_message, user.getDisplayName());
//            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            signInButton().setVisibility(View.INVISIBLE);
        } else {
            final String message = getString(R.string.sign_in_failed_message);
            Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            signInButton().setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.e("j2km", "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        switch (i) {
            case R.id.sign_in_button:
                signIn();
                break;
            case R.id.view_player_details_button:
                viewPlayerDetails();
                break;
        }
    }

    private void viewPlayerDetails() {
        if (userMostRecentlyScanned.isPresent()) {
            final Uri uri = Uri.parse(getString(R.string.app_host_url) + "/p/" + userMostRecentlyScanned.get().id().get().toKey());
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(browserIntent);
        } else {
            Toast.makeText(this, "Please scan again...", Toast.LENGTH_SHORT).show();
        }
    }

    private View signInButton() {
        return findViewById(R.id.sign_in_button);
    }

    private void showAvatar(final Drawable background) {
        avatarView().setImageDrawable(background);
        avatarView().setVisibility(View.VISIBLE);
    }

    class BackgroundImageTask extends AsyncTask<String, Void, Drawable> {

        private Exception exception;

        protected Drawable doInBackground(String... urls) {
            final Drawable result = null;
            InputStream avatarStream = null;
            if (urls != null && urls.length > 0) {
                final String urlString = urls[0];
                try {
                    //https://stackoverflow.com/a/8893865/721000 brut force escaping is the way
                    avatarStream = new URL(urlString.replace(" ", "%20")).openStream();
                    return BitmapDrawable.createFromStream(avatarStream, "avatar");
                } catch (IOException e) {
                    Log.e("422k", "unable to draw avatar background", e);
                    this.exception = e;
                } finally {
                    if (avatarStream != null) {
                        try {
                            avatarStream.close();
                        } catch (IOException e) {
                            //at least we tried
                        }
                        ;
                    }
                }
            }
            return result;
        }

        protected void onPostExecute(Drawable background) {
            if (this.exception == null && background != null) {
                showAvatar(background);
            }

        }
    }





    /**
     * Looks up the resource in icons.xml
     *
     * @param ruleId which must match the name of the drawable
     * @return the explicit icon
     */
    private View ruleIcon(final String ruleId) {
        String iconName = ruleId + "_icon";
        final int id = getResources().getIdentifier(iconName,
                "id",
                getPackageName());
        return findViewById(id);
    }

    /**
     * Shown when clicked on the associate button, this will show a dialog explaining the tag id of
     * the next scan will be associated to the current user.
     *
     * @param view
     */
    public void showTokenRewardDialog(View view) {

        final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialogInterface, final int choice) {
                switch (choice) {
                    case AlertDialog.BUTTON_NEGATIVE:
                        nextScanIsNotAssociated();
                        break;
                    default:
                        Log.w("ms29", "choice not handled: " + choice);
                }
            }
        };

        if (userMostRecentlyScanned.isPresent()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Reward a User");
            builder.setMessage("Scan a token that will reward " + userMostRecentlyScanned.get().nickname);
            builder.setNegativeButton("Cancel", listener);
            this.associateScanToCurrentUserDialog = Optional.of(builder.show());
        } else {
            Toast.makeText(this, "No user to associate", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Ensures the next scan will not be associated to the current user either because of cancelation
     * or already successful association.
     */
    private void nextScanIsNotAssociated() {
        if (associateScanToCurrentUserDialog.isPresent()) {
            AlertDialog alertDialog = associateScanToCurrentUserDialog.get();
            if (alertDialog.isShowing()) {
                alertDialog.hide();
            }
        }
        associateScanToCurrentUserDialog = Optional.absent();

    }
}
