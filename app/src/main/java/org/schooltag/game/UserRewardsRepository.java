package org.schooltag.game;

import android.support.annotation.Nullable;

import com.google.common.util.concurrent.FutureCallback;

import org.joda.time.DateTime;
import org.schooltag.entity.Path;
import org.schooltag.entity.Repository;
import org.schooltag.user.UserId;

import java.util.List;

/**
 * Created by aaronroller on 7/15/17.
 */

public final class UserRewardsRepository {

    private Repository<UserRewards, UserRewardsId> repository;
    private Repository<ScanReward, ScanRewardId> scanRewardRepository;

    public UserRewardsRepository(Repository repositoryTemplate) {
        this.repository =  repositoryTemplate.toBuilder().entityType(UserRewards.class).build();
        this.scanRewardRepository = repositoryTemplate.toBuilder().entityType(ScanReward.class).build();

    }

    /**
     * {@link UserRewards} for the user on the day given.
     *
     * @param userId   who got rewarded
     * @param dateTime any tme on the day the rules happened
     * @param callback the user rules for the day
     */
    void daily(final UserId userId,
               final DateTime dateTime,
               final FutureCallback<UserRewards> callback) {

        final Path userRewardsPath = UserRewardsPath.rewarded().to(userId).onDay(dateTime);
        userRewards(userId,userRewardsPath, callback, UserRewards.IntervalType.DAILY);
    }

    private void userRewards(final UserId userId, final Path userRewardsPath, final FutureCallback<UserRewards> callback, final UserRewards.IntervalType intervalType) {
        if(callback != null) {
            final Path path = repository.getUtils().path(this.repository.getRoot()).and(userRewardsPath);


            repository.get(new UserRewardsId(path.toString()), new FutureCallback<UserRewards>() {
                @Override
                public void onSuccess(final UserRewards result) {
                    callback.onSuccess(result.toBuilder().userId(userId).intervalType(intervalType).build());
                }

                @Override
                public void onFailure(final Throwable t) {
                    callback.onFailure(t);
                }
            });
        }
    }

    /**
     * @param userId   who got rewarded
     * @param dateTime any time during the week the rules happened
     * @param callback provides the user rules for the week
     */
    void weekly(final UserId userId, DateTime dateTime, @Nullable final FutureCallback<UserRewards> callback) {
        userRewards(userId,UserRewardsPath.rewarded().to(userId).onWeek(dateTime), callback, UserRewards.IntervalType.WEEKLY);

    }

    void yearly(final UserId userId, DateTime dateTime, final FutureCallback<UserRewards> callback) {
        userRewards(userId,UserRewardsPath.rewarded().to(userId).onYear(dateTime), callback, UserRewards.IntervalType.YEARLY);

    }

    void scanRewardsForDay(final UserId userId, DateTime dateTime, final FutureCallback<List<ScanReward>> callback){
        final Path path = UserRewardsPath.scansFor(userId).onDay(dateTime);
        scanRewardRepository.list(path,callback);
        //                                    final DatabaseReference userRewardsRef = database.getReference().child(tag.userId().toString());
//                                    userRewardsRef.child("scans").child(year).child(week).child(day).child(scanId).addValueEventListener(new ValueEventListener() {

    }

}
