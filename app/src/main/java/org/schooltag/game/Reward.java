package org.schooltag.game;

import org.schooltag.entity.Entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Individual achievement identifying the rule and associated points.
 *
 * Created by aaronroller on 8/20/17.
 */
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Builder(toBuilder = true)
public class Reward {

    public final Integer points;

}
