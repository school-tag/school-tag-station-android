package org.schooltag.game;

import org.schooltag.entity.Id;

/**
 *
 * Created by aaronroller on 7/15/17.
 */

public class UserRewardsId extends Id<UserRewards> {
    public UserRewardsId(String value) {
        super(value);
    }
}
