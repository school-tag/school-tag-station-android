package org.schooltag.game;

import com.google.common.base.Optional;
import com.google.common.util.concurrent.FutureCallback;

import org.schooltag.entity.EntityNotFoundException;
import org.schooltag.entity.RepositoryUtils;
import org.schooltag.lang.ResourceException;
import org.schooltag.scan.Scan;
import org.schooltag.scan.ScanId;
import org.schooltag.scan.ScanService;
import org.schooltag.station.R;
import org.schooltag.tag.Tag;
import org.schooltag.tag.TagId;
import org.schooltag.tag.TagService;
import org.schooltag.user.User;
import org.schooltag.user.UserId;
import org.schooltag.user.UserService;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NonNull;

/**
 * Access to {@link UserRewards} and other game features like points, coins, etc.
 * <p>
 * Created by aaronroller on 7/15/17.
 *
 * @author aaronroller
 */
@AllArgsConstructor
@Builder
public class GameService {
    @NonNull
    private UserRewardsRepository userRewardsRepository;
    @NonNull
    private UserService userService;
    @NonNull
    private ScanService scanService;
    @NonNull
    private TagService tagService;
    @NonNull
    private RepositoryUtils repositoryUtils;

    /**
     * When the android system receives a scan notification, this is called to notify
     * the system that a scan has occurred. The server determines which rules are matched
     * and provides rules.
     *
     * @param scan           the scan received on the device
     * @param userIdCallback the user associated with the tag
     */
    public void userIdForScanReceived(final Scan scan, final FutureCallback<UserScan> userIdCallback) {
        scanService.scanReceived(scan, new FutureCallback<ScanId>() {
            @Override
            public void onSuccess(final ScanId scanId) {
                //scan processed, retrieve tag for user id.
                final TagId tagId = scan.tagId().get();
                //we have to wait for it to be created.
                tagService.tag(tagId, new FutureCallback<Tag>() {
                    @Override
                    public void onSuccess(Tag tag) {
                        final Optional<UserId> userIdOptional = tag.userId();
                        final UserId userId;

                        if (userIdOptional.isPresent()) {
                            userId = userIdOptional.get();
                        }else if(scan.userIdOfRewardRecipient != null) {
                            userId = UserId.fromKey(scan.userIdOfRewardRecipient);
                        }else {
                            userId = null;
                        }

                        if(userId != null ){
                            userIdCallback.onSuccess(new UserScan(userId, scanId));
                        } else {
                            userIdCallback.onFailure(ResourceException.builder().errorCode(R.string.error_tag_without_user)
                                    .message(tagId + " from scan " + scan.id + " doesn't have a user?").build());
                        }
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        userIdCallback.onFailure(EntityNotFoundException.of(R.string.error_tag_not_found_after_scan, tagId));
                    }
                });
            }

            @Override
            public void onFailure(Throwable t) {
                userIdCallback.onFailure(ResourceException
                        .code(R.string.error_scan_failed)
                        .message(t.getMessage())
                        .cause(t).build());
            }
        });

    }

    /**
     * common scenario when the scan is received and the user wants to see the latest
     * reward status.
     *
     * FIXME: listen for changes until the next scan comes in, then deregister listening.
     *
     * @param scan
     * @param dailyRewardsCallable
     * @param weeklyRewardsCallback
     */
    public void rewardsForScanReceived(final Scan scan,
                                       final FutureCallback<List<ScanReward>> scanRewardsCallback,
                                       final FutureCallback<UserRewards> dailyRewardsCallable,
                                       final FutureCallback<UserRewards> weeklyRewardsCallback,
                                       final FutureCallback<UserRewards> yearlyRewardsCallback,
                                       final FutureCallback<User> userCallback) {
        userIdForScanReceived(scan, new FutureCallback<UserScan>() {
            @Override
            public void onSuccess(final UserScan userScan) {
                final UserId userId = userScan.userId;
                userRewardsForToday(userId, dailyRewardsCallable);
                userRewardsForWeek(userId, weeklyRewardsCallback);
                userRewardsForYear(userId, yearlyRewardsCallback);
                scanRewardsForToday(userId, scanRewardsCallback);
                userService.user(userId, userCallback);
            }

            @Override
            public void onFailure(final Throwable t) {
                if(dailyRewardsCallable != null) dailyRewardsCallable.onFailure(t);
                if(weeklyRewardsCallback != null) weeklyRewardsCallback.onFailure(t);
                if(yearlyRewardsCallback != null) yearlyRewardsCallback.onFailure(t);
                if(scanRewardsCallback != null) scanRewardsCallback.onFailure(t);
                if(userCallback != null) userCallback.onFailure(t);
            }
        });
    }

    public void userRewardsForToday(final UserId userId, final FutureCallback<UserRewards> dailyRewardsCallable) {
        userRewardsRepository.daily(userId, repositoryUtils.now(), dailyRewardsCallable);
    }

    public void userRewardsForWeek(final UserId userId, final FutureCallback<UserRewards> callback) {
        userRewardsRepository.weekly(userId, repositoryUtils.now(), callback);
    }

    public void userRewardsForYear(final UserId userId, final FutureCallback<UserRewards> callback) {
        userRewardsRepository.yearly(userId, repositoryUtils.now(), callback);
    }

    public void scanRewardsForToday(final UserId userId, final FutureCallback<List<ScanReward>> callback) {
        userRewardsRepository.scanRewardsForDay(userId, repositoryUtils.now(), callback);
    }

}
