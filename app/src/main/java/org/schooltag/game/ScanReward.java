package org.schooltag.game;

import org.schooltag.entity.Entity;

import java.util.List;
import java.util.Map;

import lombok.Builder;
import lombok.NoArgsConstructor;

/**The rules given for a single scan.
 * Created by aaronroller on 8/20/17.
 */
@NoArgsConstructor(force = true)
public class ScanReward extends Entity<ScanReward, ScanRewardId> {

    public final Map<String,Reward> rules;

    @Builder(toBuilder = true)
    ScanReward(final String id, final Map<String,Reward> rules) {
        super(id);
        this.rules = rules;
    }
}
