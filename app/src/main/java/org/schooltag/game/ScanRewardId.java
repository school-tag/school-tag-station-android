package org.schooltag.game;

import org.schooltag.entity.Id;

import lombok.NonNull;

/**
 * Created by aaronroller on 8/20/17.
 */

public class ScanRewardId extends Id<ScanReward> {
    public ScanRewardId(@NonNull final String value) {
        super(value);
    }

    /**Just the part of the id that represents the scan
     *
     * @return
     */
    public String scanKey() {
        return key();
    }
}
