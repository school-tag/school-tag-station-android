package org.schooltag.game;

import org.schooltag.scan.ScanId;
import org.schooltag.user.UserId;

import lombok.AllArgsConstructor;

/**Simple pair to indicate the user identified for the scan.
 *
 * Created by aaronroller on 8/21/17.
 */
@AllArgsConstructor
public class UserScan {
    public final UserId userId;
    public final ScanId scanId;
}
