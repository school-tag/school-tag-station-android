package org.schooltag.game;

import android.support.annotation.Nullable;

import org.schooltag.entity.Entity;
import org.schooltag.user.UserId;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Points, coins and badges rewarded for scans and cumulative achievements.
 * Represents a certain time interval like a day, a week, a month or year.
 *
 * Created by aaronroller on 7/15/17.
 */
@NoArgsConstructor(force = true)
public class UserRewards extends Entity<UserRewards, UserRewardsId> {

    public final Integer points;
    public final Integer coins;
    public final IntervalType intervalType;
    private final UserId userId;
    @Builder(toBuilder = true)
    UserRewards(@Nullable String id,
                @Nullable UserId userId,
                @NonNull Integer points,
                @NonNull Integer coins,
                @Nullable IntervalType intervalType) {
        super(id);
        this.points = points;
        this.coins = coins;
        this.intervalType = intervalType;
        this.userId = userId;
    }


    public UserId rewardUserId(){
        return this.userId;
    }

    /**
     * Indicates the type of time span these rules represent.
     */
    enum IntervalType {
        DAILY,
        WEEKLY,
        YEARLY
    }

}
