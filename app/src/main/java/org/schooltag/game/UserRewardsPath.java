package org.schooltag.game;

import org.joda.time.DateTime;
import org.schooltag.entity.Path;
import org.schooltag.entity.Paths;
import org.schooltag.user.UserId;
import org.schooltag.user.UsersPath;

/**
 * Provides the standard restful database paths to the data for the user rules.
 *
 * Created by aaronroller on 7/22/17.
 */

class UserRewardsPath extends Path {

    private static final String REWARDS = "rewards";
    private static final String TOTALS_PATH = "totals";
    private static final String DAILIES_PATH = "dailies";
    private static final String WEEKLIES_PATH = "weeklies";
    private static final String YEARLIES_PATH = "yearlies";
    private static final String POINTS_PATH = "points";
    private static final String SCANS_PATH = "scans";

    private UserRewardsPath() {
        super(REWARDS);
    }

    static UserRewardsPath rewarded() {
        return new UserRewardsPath();
    }

    static ScansPath scansFor(UserId userId) {
        final UserRewardsPath root = new UserRewardsPath();
        return root.new ScansPath(UsersPath.from(root).id(userId));
    }

    public TotalsPath to(UserId userId) {
        return new TotalsPath(UsersPath.from(this).id(userId));
    }

    /**
     * Paths to summaries for time intervals.
     */
    class TotalsPath extends Path {

        TotalsPath(final Path path) {
            super(path);
        }

        /**
         * @param dateTime of interest
         * @return Path to the totals for a day.
         */
        Path onDay(DateTime dateTime) {
            return and(TOTALS_PATH).and(DAILIES_PATH).and(Paths.day(dateTime));
        }

        /**
         * @param dateTime of interest
         * @return Path to the totals for a week.
         */
        public Path onWeek(DateTime dateTime) {
            return and(TOTALS_PATH).and(WEEKLIES_PATH).and(Paths.week(dateTime));
        }

        public Path onYear(DateTime dateTime) {
            return and(TOTALS_PATH).and(YEARLIES_PATH).and(Paths.year(dateTime));
        }
    }

    class ScansPath extends Path {
        ScansPath(final Path path) {
            super(path.and(SCANS_PATH));
        }

        Path onDay(DateTime dateTime) {
            return and(Paths.day(dateTime));
        }
    }


}
