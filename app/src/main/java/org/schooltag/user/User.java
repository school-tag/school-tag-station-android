package org.schooltag.user;

import org.schooltag.entity.Entity;

import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/**
 * Created by aaronroller on 7/15/17.
 */
@NoArgsConstructor(force = true)
public class User extends Entity<User, UserId> {

    public final String nickname;
    public final String avatar;

    @Builder(toBuilder = true)
    public User(String id, String nickname, String avatar) {
        super(id);
        this.nickname = nickname;
        this.avatar = avatar;
    }


}
