package org.schooltag.user;

import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

import org.schooltag.entity.Path;

import java.util.StringTokenizer;

import lombok.Builder;

/**
 * Created by aaronroller on 7/22/17.
 */

public class UsersPath extends Path {
    private static final String USERS = "users";

    protected UsersPath(Path root) {
        super(root.toString());
    }

    public static UsersPath from(Path root) {
        return new UsersPath(root);
    }

    public static final Path fromKey(String key) {
        return UsersPath.from().and(USERS).and(key);
    }


    public final Path id(UserId userId) {
        final String key = key(userId);
        return and(USERS).and(key);
    }

    public static String key(final UserId userId) {
        return Iterables.getLast(Splitter.on("/").omitEmptyStrings().split(userId.toString()));
    }

}
