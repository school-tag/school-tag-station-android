package org.schooltag.user;

import lombok.NonNull;

import org.schooltag.entity.Id;

/**
 * Created by aaronroller on 7/16/17.
 */

public class UserId extends Id<User> {
    public UserId(@NonNull String value) {
        super(value);
    }

    public static UserId fromKey(String key) {
        return new UserId(UsersPath.fromKey(key).toString());
    }

   public String toKey(){
       return UsersPath.key(this);
   }
}
