package org.schooltag.user;

import com.google.common.util.concurrent.FutureCallback;
import com.google.firebase.database.DatabaseReference;

import org.schooltag.entity.Repository;
import org.schooltag.entity.RepositoryUtils;

import lombok.Builder;
import lombok.NonNull;

/**
 * Created by aaronroller on 7/21/17.
 */

public class UserRepository  {

    private Repository<User, UserId> repository;

    @Builder
    public UserRepository(Repository<User,UserId> repositoryTemplate) {
        this.repository = repositoryTemplate.toBuilder().entityType(User.class).build();
    }

    /** Single value call.
     *
     * @param userId identifying the desired user
     * @param callback provides the results
     */
    public void once(UserId userId, FutureCallback<User> callback){
        //FIXME: really use once
         this.repository.get(userId,callback);
    }

}
