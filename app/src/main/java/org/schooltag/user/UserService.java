package org.schooltag.user;

import com.google.common.util.concurrent.FutureCallback;

import org.schooltag.entity.Repository;

import lombok.Builder;

/**
 * Created by aaronroller on 8/21/17.
 */

public class UserService {

    private final UserRepository userRepository;

    @Builder
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void user(UserId userId, FutureCallback<User> callback) {
        this.userRepository.once(userId, callback);
    }
}
