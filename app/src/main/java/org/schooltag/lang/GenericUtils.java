package org.schooltag.lang;

import java.lang.reflect.ParameterizedType;

/**
 * Created by aaronroller on 7/17/17.
 */

public final class GenericUtils {

    /**
     * Used to indicate which paremeter in a class declaration.
     */
    public enum Parameter {
        FIRST, SECOND, THIRD
    }

    static public <P, T> Class<T> typeFromParentParameter(Class<P> subclassOfParentWithParams,
                                                          Parameter parameterIndex) {
        return (Class) ((ParameterizedType) subclassOfParentWithParams
                .getGenericSuperclass()).getActualTypeArguments()[parameterIndex.ordinal()];
    }

}
