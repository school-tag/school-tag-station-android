package org.schooltag.lang;

import android.support.annotation.Nullable;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

/**
 * Provides reasons for an exception that can translate to the user.
 * The message is for developer feedback only.
 * Created by aaronroller on 7/22/17.
 */
public class ResourceException extends Exception {

    @Getter
    private final int errorCode;

    /**
     * @param errorCode from the Resources error codes files.
     * @param message   developer only message explaining what went wrong.. useful for the logs
     * @param cause     more detailed stack trace of the problem
     */
    @Builder
    public ResourceException(@NonNull Integer errorCode, @Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public static ResourceExceptionBuilder code(Integer errorCode) {
        return ResourceException.builder().errorCode(errorCode);
    }

}
